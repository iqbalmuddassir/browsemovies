package muddassir.com.browsemovie.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import muddassir.com.browsemovie.R;
import muddassir.com.browsemovie.adapter.MovieAdapter;
import muddassir.com.browsemovie.constant.AppConstants;
import muddassir.com.browsemovie.retrofit.MovieResponse;
import muddassir.com.browsemovie.retrofit.RestClient;
import muddassir.com.browsemovie.retrofit.Search;
import muddassir.com.browsemovie.view.FullView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvResponse;
    EditText etSearch;
    MovieAdapter adapter;
    FullView fvPoster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
    }

    private void initComponents() {
        etSearch = (EditText) findViewById(R.id.etSearch);
        rvResponse = (RecyclerView) findViewById(R.id.rvResponse);
        fvPoster = (FullView) findViewById(R.id.fvPoster);

        adapter = new MovieAdapter(this);
        rvResponse.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvResponse.setAdapter(adapter);

        setTextChangeListener();

        setRecyclerItemTouchListener();
    }

    private void setRecyclerItemTouchListener() {
        ItemTouchHelper.Callback callback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                adapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Search movie = adapter.onItemDismiss(viewHolder.getAdapterPosition());
                fvPoster.setMovie(movie);
                fvPoster.setVisibility(View.VISIBLE);
            }

        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(rvResponse);
    }

    private void setTextChangeListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString()) && s.length() > 2) {
                    sendSearchQuery(s.toString());
                }
            }
        });
    }

    private void sendSearchQuery(String query) {
        RestClient.createService().searchMovies(query, AppConstants.RESPONSE_TYPE, new Callback<MovieResponse>() {
            @Override
            public void success(MovieResponse movieResponse, Response response) {
                if (movieResponse != null && movieResponse.getSearch() != null) {
                    adapter.setMovies(movieResponse.getSearch());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "No search result for this request", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
