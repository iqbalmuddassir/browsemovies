package muddassir.com.browsemovie.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import muddassir.com.browsemovie.R;
import muddassir.com.browsemovie.retrofit.Search;

/**
 * @author muddassir on 9/7/16.
 */
public class FullView extends RelativeLayout {
    ImageView banner;
    TextView title, description;

    public FullView(Context context) {
        super(context);
        init(context);
    }

    public FullView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (inflater != null) {
            inflater.inflate(R.layout.movie_full_view, this);
            banner = (ImageView) findViewById(R.id.banner);
            title = (TextView) findViewById(R.id.title);
            description = (TextView) findViewById(R.id.description);
        }
    }

    public void setBannerUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(getContext()).load(url).into(banner);
        }
    }

    public void setTitle(String text) {
        if (!TextUtils.isEmpty(text)) {
            title.setText(text);
        }
    }

    public void setDescription(String text) {
        if (!TextUtils.isEmpty(text)) {
            description.setText(text);
        }
    }

    public void setMovie(Search movie) {
        setBannerUrl(movie.getPoster());
        setTitle(movie.getTitle());
        setDescription(movie.getType());
    }
}
