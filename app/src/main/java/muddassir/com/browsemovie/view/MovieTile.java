package muddassir.com.browsemovie.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import muddassir.com.browsemovie.R;

/**
 * @author muddassir on 9/7/16.
 */
public class MovieTile extends RelativeLayout {
    ImageView banner;
    TextView title;

    public MovieTile(Context context) {
        super(context);
        init(context);
    }

    public MovieTile(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (inflater != null) {
            inflater.inflate(R.layout.movie_tile_view, this);
            banner = (ImageView) findViewById(R.id.banner);
            title = (TextView) findViewById(R.id.title);
        }
    }

    public void setBannerUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(getContext()).load(url).into(banner);
        }
    }

    public void setTitle(String text) {
        if (!TextUtils.isEmpty(text)) {
            title.setText(text);
        }
    }
}
