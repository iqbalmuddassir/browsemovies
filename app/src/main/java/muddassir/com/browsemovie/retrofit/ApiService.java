package muddassir.com.browsemovie.retrofit;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

import static muddassir.com.browsemovie.constant.AppConstants.RESPONSE_QUERY;
import static muddassir.com.browsemovie.constant.AppConstants.SEARCH_QUERY;

/**
 * @author muddassir on 9/7/16.
 */
public interface ApiService {
    @GET("/")
    void searchMovies(
            @Query(SEARCH_QUERY) String searchText,
            @Query(RESPONSE_QUERY) String responseType,
            Callback<MovieResponse> callback
    );
}
