package muddassir.com.browsemovie.retrofit;

import com.squareup.okhttp.OkHttpClient;

import muddassir.com.browsemovie.constant.AppConstants;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * @author muddassir on 9/7/16.
 */
public class RestClient {
    private static RestAdapter.Builder builder = new RestAdapter.Builder()
            .setEndpoint(AppConstants.BASE_URL)
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .setClient(new OkClient(new OkHttpClient()));

    public static ApiService createService() {
        RestAdapter adapter = builder.build();
        return adapter.create(ApiService.class);
    }
}