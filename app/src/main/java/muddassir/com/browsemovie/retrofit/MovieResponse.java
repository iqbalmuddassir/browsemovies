package muddassir.com.browsemovie.retrofit;

/**
 * @author muddassir on 9/7/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MovieResponse {

    @SerializedName("Search")
    @Expose
    private List<Search> search = new ArrayList<Search>();
    @SerializedName("totalResults")
    @Expose
    private String totalResults;
    @SerializedName("Response")
    @Expose
    private String response;
    public List<Search> getSearch() {
        return search;
    }
    public String getTotalResults() {
        return totalResults;
    }
    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }
    public String getResponse() {
        return response;
    }
}
