package muddassir.com.browsemovie.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author muddassir on 9/7/16.
 */
public class Search {

    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Year")
    @Expose
    private String year;
    @SerializedName("imdbID")
    @Expose
    private String imdbID;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Poster")
    @Expose
    private String poster;
    public String getTitle() {
        return title;
    }
    public String getYear() {
        return year;
    }
    public String getImdbID() {
        return imdbID;
    }
    public String getType() {
        return type;
    }
    public String getPoster() {
        return poster;
    }
}
