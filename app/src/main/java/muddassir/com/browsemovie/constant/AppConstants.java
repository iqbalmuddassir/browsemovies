package muddassir.com.browsemovie.constant;

/**
 * @author muddassir  on 9/7/16.
 */
public interface AppConstants {
    String BASE_URL = "http://www.omdbapi.com/";
    String SEARCH_QUERY = "s";
    String RESPONSE_QUERY = "r";
    String RESPONSE_TYPE = "json";
}
