package muddassir.com.browsemovie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import muddassir.com.browsemovie.retrofit.Search;
import muddassir.com.browsemovie.view.MovieTile;

/**
 * @author muddassir on 9/7/16.
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {

    List<Search> movies = new ArrayList<>();
    Context context;

    public MovieAdapter(Context context) {
        this.context = context;
    }

    @Override
    public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieHolder(new MovieTile(context));
    }

    @Override
    public void onBindViewHolder(MovieHolder holder, int position) {
        holder.movieTiles.setBannerUrl(movies.get(position).getPoster());
        holder.movieTiles.setTitle(movies.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return movies == null ? 0 : movies.size();
    }

    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(movies, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(movies, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    public Search onItemDismiss(int position) {
        Search movie = movies.remove(position);
        notifyItemRemoved(position);
        return movie;
    }

    public static class MovieHolder extends RecyclerView.ViewHolder {
        MovieTile movieTiles;

        public MovieHolder(View itemView) {
            super(itemView);
            movieTiles = (MovieTile) itemView;
        }
    }

    public void setMovies(List<Search> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }
}
